require 'active_model'

module SMS
  class Message
    include ActiveModel::Model

    attr_accessor :seunum
    attr_accessor :celular
    attr_accessor :mensagem
    attr_accessor :status
    attr_accessor :datarec, :dataenv, :datastatus
    attr_accessor :op
  end
end
