require 'thor'
require_relative './api'

module SMS
  class CLI < Thor
    desc 'credits', 'Verifica os créditos de um Usuário Pré-Pago'
    def credits
      puts api.credits
    end

    desc 'validity', 'Retorna a data de validade dos créditos de um Usuário Pré-Pago'
    def validity
      puts api.validity
    end

    desc 'send NUMBER MESSAGE', 'Envia MESSAGE para um celular NUMBER'
    def send(number, message)
      puts api.send_sms(number, message)
    end

    desc 'get_sent', 'Retorna as mensagens transmitidas dentro de um período MÁXIMO DE 4 DIAS, e um MÁXIMO DE 4000 SMSs'
    def get_sent
      puts api.get_sent
    end

    desc 'get_unread', 'Retorna as mensagens não lidas'
    def get_unread
      puts api.get_unread
    end

    private
    def api
      @api ||= Api.new
    end
  end
end
