require 'net/http'
require 'nokogiri'
require 'uri'

module SMS
  class Api
    HOST = 'https://webservices.twwwireless.com.br'
    NS = 'https://www.twwwireless.com.br/reluzcap/wsreluzcap'

    def credits
      parse_int call('VerCredito')
    end

    def validity
      parse call('VerValidade'), 'dateTime'
    end

    def send_sms(number, message)
      response = call('EnviaSMS', {
        'SeuNum' => '0800', # TODO nao parece fazer diferença
        'Celular' => number,
        'Mensagem' => URI.encode(message)
      })

      parse response, 'string'
    end

    def get_sent
      response = call('BuscaSMS', {
        'DataIni' => '2015-03-26',
        'DataFim' => '2015-03-29'
      })

      parse_sms(response.xpath("//OutDataSet/BuscaSMS"))
    end

    def get_unread
      response = call('BuscaSMSMONaoLido')
      parse_sms(response.xpath("//OutDataSet/SMSMO"))
    end

    private
    def username
      ENV['WS_USERNAME']
    end

    def password
      ENV['WS_PASSWORD']
    end

    def call(action, params={})
      endpoint = HOST + "/reluzcap/wsreluzcap.asmx/#{action}?NumUsu=#{username}&Senha=#{password}"
      endpoint += "&" + params.map { |p| p.join("=") }.join("&") unless params.empty?

      response = Net::HTTP.get_response(URI.parse(endpoint))
      raise Exception.new(response.body) if response.code == "500"
      Nokogiri::XML(response.body)
    end

    def parse(response, result_data_type)
      response.xpath("ns:#{result_data_type}", ns: NS).text
    end

    def parse_int(response)
      parse(response, 'int').to_i
    end

    def parse_sms(elements)
      elements.map {|sms|
        e = sms.children
        .select { |c| c.class == Nokogiri::XML::Element}
        .map {|c| {c.name => c.text.strip }}
        .flatten

        SMS::Message.new(
          seunum: fetch(e, 'seunum'),
          celular: fetch(e, 'celular'),
          mensagem: fetch(e, 'mensagem'),
          status: fetch(e, 'status'),
          datarec: fetch(e, 'datarec'),
          dataenv: fetch(e, 'dataenv'),
          datastatus: fetch(e, 'datastatus'),
          op: fetch(e, 'op')
        )
      }
    end

    def fetch(hash_array, key)
      hash = hash_array.find {|h| h.has_key?(key) }
      hash.values.first
    end
  end
end
