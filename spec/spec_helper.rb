require './lib/sms.rb'

require 'vcr'
require 'webmock/rspec'

RSpec.configure do |config|
  config.mock_with :rspec
end

VCR.configure do |config|
  config.cassette_library_dir = 'spec/cassettes'
  config.hook_into :webmock
  config.configure_rspec_metadata!
end
