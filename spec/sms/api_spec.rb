require 'spec_helper'

describe SMS::Api do
  context 'sending SMS' do
    it 'returns OK', vcr: {cassette_name: 'sms_send'} do
      res = subject.send_sms('5193775569', 'hello world')
      expect(res).to eq('OK')
    end

    it 'returns NOK when missing data', vcr: {cassette_name: 'sms_send_err'} do
      res = subject.send_sms(nil, 'message')
      expect(res).to eq('NOK')
    end
  end

  context 'reading SMS' do
    it 'reads sent messages', vcr: {cassette_name: 'get_sent_messages'} do
      res = subject.get_sent
      expect(res.size).to eq(2)
    end

    context 'unread' do
      it 'reads messages', vcr: {cassette_name: 'get_unread_messages'} do
        res = subject.get_unread
        expect(res.size).to be(1)
      end

      it 'returns proper message object', vcr: {cassette_name: 'get_unread_messages'} do
        res = subject.get_unread

        msg = res.first

        expect(msg.seunum).to eq('0800')
        expect(msg.celular).to eq('555193775569')
        expect(msg.mensagem).to eq('Ralou')
        expect(msg.status).to eq('MO')
        expect(msg.datarec).to eq('2015-03-29T23:50:22.443-03:00')
        expect(msg.dataenv).to eq('2015-03-29T23:50:24.427-03:00')
        expect(msg.datastatus).to eq('2015-03-29T23:50:24.427-03:00')
        expect(msg.op).to eq('3')
      end

      it 'no messages', vcr: {cassette_name: 'get_unread_no_messages'} do
        res = subject.get_unread
        expect(res).to be_empty
      end
    end
  end

  context 'credits', vcr: {cassette_name: 'credits'} do
    it 'returns remaining credits' do
      expect(subject.credits).to eq(499)
    end
  end

  context 'validity', vcr: {cassette_name: 'validity'} do
    it 'returns validity date' do
      expect(subject.validity).to eq('2015-05-25T00:00:00')
    end
  end
end
