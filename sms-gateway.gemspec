Gem::Specification.new do |s|
  s.name        = 'sms-gateway'
  s.version     = '0.0.1'
  s.date        = '2015-04-21'
  s.summary     = 'SMS api'
  s.description = 'Simple api to send and receive SMS.'
  s.authors     = ['Fernando Witzke']
  s.email       = 'pigroxalot@gmail.com'
  s.files       = ['lib/sms.rb', 'lib/sms/api.rb', 'lib/sms/cli.rb']
  s.homepage    = 'http://rubygems.org/gems/sms-gateway'
  s.license     = 'MIT'

  s.add_runtime_dependency 'nokogiri', '~> 1.6', '>= 1.6.6'
  s.add_runtime_dependency 'dotenv', '~> 2.0'
  s.add_runtime_dependency 'thor', '~> 0.19', '>= 0.19.1'
end
