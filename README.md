# SMS gateway

Command line tool to enable sending/receiving SMS.

supported providers
- TWW reluzcap

## How to use
First you need to set environment variables with username/password.
```
export WS_USERNAME=<USERNAME>
export WS_PASSWORD=<PASSWORD>
```

Then just run:
```bash
./bin/sms
Commands:
  sms credits              # Verifica os créditos de um Usuário Pré-Pago
  sms get_sent             # Retorna as mensagens transmitidas dentro de um período MÁXIMO DE 4 DIAS, e um MÁXIMO DE 4000 SMSs
  sms get_unread           # Retorna as mensagens não lidas
  sms help [COMMAND]       # Describe available commands or one specific command
  sms send NUMBER MESSAGE  # Envia MESSAGE para um celular NUMBER
  sms validity             # Retorna a data de validade dos créditos de um Usuário Pré-Pago
```
